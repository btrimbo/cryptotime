#!/usr/bin/env python3

def letter_count(str1):
    str2 = ""
    cdict = {}
    str1 = str1.casefold().replace(" ", "").replace(".", "").replace("?", "").replace("!", "").replace(",", "").replace(
        ";", "").replace(":", "").replace("'", "").replace('"', "")
    for i in str1:
        cdict[i] = i
    for k, v in cdict.items():
        str2 = str2 + cdict[v]
    for i in str2:
        cdict[i] = str1.count(i)
    char_counts = sorted(cdict.items(), key=lambda char: char[1], reverse=True)
    print()
    print("Letter Counts")
    print("",end="")
    for k, v in char_counts:
        print(f'{k:1}:{v:1}', end=" ")
    print()

def build_d(str1):
    p = 0
    str2 = {}
    for i in str1:
        if i == ' ':
            str2[p] = ' '
        elif i == '.' or i == '!' or i == '?' or i == ',':
            str2[p] = i
        else:
            str2[p] = "-"
        p += 1
    return str2

def guess_letter(lg,lr,str1,str2):
    p = 0
    for i in str1:
        if i == lr:
            str2[p] = lg
        p += 1
    return

def main():
    encryptedText = ""
    tempf = ""
    wkgl = {}

    menu = {}
    menu['1'] = "Input Cyphertext."
    menu['2'] = "Reset Guesses."
    menu['3'] = "Begin Solving."
    menu['4'] = "Exit."
    while True:
        options = menu.keys()
        sorted(options)
        print()
        for entry in options:
            print(entry, menu[entry])
        print()
        print(tempf)
        if wkgl != {}:
            for k, v in wkgl.items():
                print(v, end='')
            print()
            print()
        selection = input("Please Select: ")
        if selection == '1':
            tempf = input("Paste the cyphertext here: ")
            encryptedText = tempf
            wkgl = build_d(tempf)
        elif selection == '2':
            tempf = encryptedText
            wkgl = build_d(tempf)
        elif selection == '3':
            letter = "0"
            try:
                while letter != "1":
                    print()
                    print("|`````````````|")
                    print("|    SOLVE    |")
                    print("|_____________|")
                    print()
                    print("note: guesses are case sensitive")
                    print()
                    print(tempf)
                    for k, v in wkgl.items():
                        print(v, end='')
                    print(" ")
                    letter_count(tempf)
                    print()
                    repl = input("enter 1 to quit\r\n" + "enter 2 to reset your guesses\r\n" + "or enter the letter you wish to replace\r\n" + "entry: ")
                    letter = repl
                    print()
                    if letter == '2':
                        tempf = encryptedText
                        wkgl = build_d(tempf)
                    elif letter == '1':
                        continue
                    else:
                        guess = input("enter your guess: ")
                        guess_letter(guess,repl,tempf,wkgl)
                    print()
            except:
                print("That input is not valid.")
        elif selection == '4':
            break
        else:
            print()
            print("Unknown Option Selected!")
            print()

if __name__ == "__main__":
    main()